import React from 'react';

import PagePicker from './components/PagePicker.js'
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification'


var keyrequest;
var appComponent;

export const LocalNotification = () => {
    PushNotification.localNotification({
        autoCancel: true,
        channelId: "tokenrequest",
        priority: 'urgent',
        bigText:
        'Click to select account, or autofill with the button below.',
        title: 'Token Requested',
        message: 'Expand to Send or Fill',
        vibrate: true,
        vibration: 500,
        playSound: true,
        soundName: 'default',
        actions: '["Fill"]'
    })
}


// Handle Background Notification Messages
messaging().setBackgroundMessageHandler(async remoteMessage => {
    // Create the Channel
    PushNotification.createChannel(
        {
            channelId: "tokenrequest", // (required)
            channelName: "Token Request", // (required)
            playSound: true,
            soundName: "default",
            importance: 4,
            vibrate: true,
        },
        (created) => console.log(`createChannel returned '${created}'`)
    );


    // Configure the notification and actions/callbacks
    PushNotification.configure({
        onNotification: function(notification) {
            // On user pressing "Fill"
            if (notification.action == "Fill") {
                keyrequest = keyrequest.replace("Request:", "Fill:")
                keyrequest = keyrequest.replace("Assoc:", "AssocFill:")
                keyrequest = keyrequest.replace("ExtraRequest:", "ExtraFill:")
                appComponent.reRender();
            // On user pressing the notification
            } else if (notification.priority) {
                appComponent.reRender();
            }
        },

        popInitialNotification: true,
        requestPermissions: true
    })
    LocalNotification()

    keyrequest = remoteMessage.data.keyrequest

});
//<PagePicker key={appKey.toString()}  keyrequest={keyrequest}/>

// New key must be generated for every re-render
var appKey = 1;

class App extends React.Component {
    constructor() {
        super();
        appComponent = this;
    }

    reRender = () => {
        appKey++
        this.forceUpdate();
    }

    render() {
        return (
            <>
            <PagePicker key={appKey.toString()}  keyrequest={keyrequest}/>
            </>
        );
    }
}

export default App;
