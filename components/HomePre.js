import React from 'react';
import {Text, Image, TouchableHighlight} from 'react-native';
import QrImage from './images/small_qr_icon.png'
import QrImageDark from './images/small_qr_icon_dark.png'



export default class HomePre extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			textColor: props.dark ? '#f7f9f9' : "#525060"
		}
		this.qriconClick = this.qriconClick.bind(this);
	}

    // On QR Icon click
	qriconClick() {
		this.props.mainCallBack.bind(this)("Camera")
	}

    // Check for change in dark on update
	componentDidUpdate(prevProps) {
		if (prevProps.dark !== this.props.dark) {
			this.setState({	textColor: this.props.dark ? '#f7f9f9' : "#525060"})
		}
	}

	render() {
		return (
			<>
				<Text style={{marginTop: 50, textAlign: 'center', fontSize: 30, fontFamily: 'CentraNo2-Medium', color: this.state.textColor}}>{this.props.textColor}Scan the QR code in the browser extention</Text>
				{this.state.test}
				<TouchableHighlight underlayColor={'transparent'} onPress={this.qriconClick} style={{width: '50%', height: 'auto', left: '25%', top: '4%'}}>
					<Image style={{width: "100%", height: undefined, aspectRatio: 0.915}} source={this.props.dark ? QrImageDark : QrImage}/>
				</TouchableHighlight>
				<Text style={{width: '90%', left: '5%', marginTop: 50, textAlign: 'center', fontSize: 20, fontFamily: 'CentraNo2-Medium', color: this.state.textColor}}>If you are yet to install the Browser Extention, Simply search for:{"\n"}"Better Authenticator Extention"{"\n"}for Chrome or Edge, or Firefox.</Text>
			</>
    );
  }
}
