import React from 'react';
import {Image, TouchableHighlight, View} from 'react-native';
import HomePre from './HomePre';
import HomePost from './HomePost';
import Test from './Test'
import Accounts from './Accounts';
import Home from './images/small_home.png'
import HomeDark from './images/small_home_dark.png'
import Connect from './images/small_connect.png'
import ConnectDark from './images/small_connect_dark.png'
import Settings from './images/small_settings.png'
import SettingsDark from './images/small_settings_dark.png'
import HomeGrey from './images/small_home_grey.png'
import HomeGreyDark from './images/small_home_grey_dark.png'
import ConnectGrey from './images/small_connect_grey.png'
import ConnectGreyDark from './images/small_connect_grey_dark.png'
import SettingsGrey from './images/small_settings_grey.png'
import SettingsGreyDark from './images/small_settings_grey_dark.png'

import styled from 'styled-components'


const Separator = styled.View`
	position: absolute;
	bottom: 80px;
	left: 5%;
	width: 90%;
	height: 3px;
	border-bottom-right-radius: 1.5px;
	border-bottom-left-radius: 1.5px;
	border-top-left-radius:  1.5px;
	border-top-right-radius:  1.5px;
`;

export default class Main extends React.Component {
	constructor(props) {
        super(props);
				this.state = {
					dark: props.dark,
					connected: props.connected,
					color: props.dark ? '#e0e2e2' : "#525060",
					viewname: 'accounts'
				}
				this.changeViewHome = this.changeViewHome.bind(this);
				this.changeViewAccounts = this.changeViewAccounts.bind(this);
				this.changeViewSettings = this.changeViewSettings.bind(this);
	}

    // Always show the Accounts on mount
	componentDidMount()  {
		this.changeViewAccounts()
	}

	componentDidUpdate(prevProps) {
        // Update dark on props update
		if (prevProps.dark !== this.props.dark) {
			this.setState({dark: this.props.dark})
		}
        // Update home on update depending on if connected or not
		if (prevProps.connected !== this.props.connected) {
			this.setState({connected: this.props.connected})
			if (this.props.connected) {
				this.setState({viewname: 'accounts'})
			} else {
				this.setState({viewname: 'homepre'})
			}
		}
	}

    // On home/link press
	changeViewHome() {
        // Change home depending on if connected or not
		if (this.state.connected) {
			this.setState({viewname: 'homepost'})
		} else {
			this.setState({viewname: 'homepre'})
		}
	}
    // On accounts press
	changeViewAccounts() {
			this.setState({viewname: 'accounts'})
	}
    // On Settings press
	changeViewSettings() {
			this.setState({viewname: 'test'})
	}

	render() {
		return (
			<>
				<View style={{height: '100%', backgroundColor: this.state.dark ? "#525060" : "#f7f9f9"}}>
					{(this.state.viewname == "accounts") && <Accounts dark={this.props.dark} mainCallBack={this.props.pickerCallBack}/>}
					{(this.state.viewname == "homepre") && <HomePre dark={this.state.dark} mainCallBack={this.props.pickerCallBack}/>}
					{(this.state.viewname == "homepost") && <HomePost dark={this.state.dark} mainCallBack={this.props.pickerCallBack}/>}
					{(this.state.viewname == "test") && <Test dark={this.state.dark} mainCallBack={this.props.pickerCallBack}/>}

					<Separator style={{backgroundColor: this.state.dark ? "#e0e2e2" : "#525060",borderColor: this.state.color}}/>
					<View style={[{position: 'absolute', width: '100%', bottom: 0, maxHeight: 60, flex: 1, flexDirection: 'row', alignSelf: 'stretch'}]}>

						<TouchableHighlight underlayColor={'transparent'} onPress={this.changeViewHome}
							style={[{width: '33.3%', alignSelf: 'stretch'}]}>
							<View style={{width: '100%'}}>
								<Image style={{flex:1, width: 'auto', height: 60, marginBottom: 40, top: -10, resizeMode: 'contain'}} source={this.state.viewname.indexOf("home") != -1 ? this.props.dark ? ConnectDark : Connect : this.props.dark ? ConnectGreyDark : ConnectGrey}/>
							</View>
						</TouchableHighlight>
						<TouchableHighlight underlayColor={'transparent'} onPress={this.changeViewAccounts}
							style={[{width: '33.3%', alignSelf: 'stretch'}]}>
							<View onPress={this.changeViewAccounts} style={{width: '100%'}} >
								<Image style={{flex:1, width: 'auto', height: 60, top: 5, marginBottom: 40, top: -10, resizeMode: 'contain'}} source={this.state.viewname == "accounts" ? this.props.dark ? HomeDark : Home : this.props.dark ? HomeGreyDark : HomeGrey}/>
							</View>
						</TouchableHighlight>
						<TouchableHighlight underlayColor={'transparent'} onPress={this.changeViewSettings}
							style={[{width: '33.3%', alignSelf: 'stretch'}]}>
							<View >
								<Image style={{flex:1, width: 'auto', height: 60, top: 5, marginBottom: 40, top: -10, resizeMode: 'contain'}} source={this.state.viewname == "test" ? this.props.dark ? SettingsDark : Settings : this.props.dark ? SettingsGreyDark : SettingsGrey}/>
							</View>
						</TouchableHighlight>
					</View>
				</View>
			</>
    );
  }
}
