import React from 'react';
import {AppState, Alert, Text, Dimensions, ToastAndroid, TouchableHighlight, View, Clipboard} from 'react-native';
import totp from 'totp-generator'
import styled from 'styled-components'
import * as Keychain from 'react-native-keychain';


const CircleBig = styled.View`
	width: 0px;
	height: 0px;
	padding: 0;
	position: absolute;
	border-radius: 20px;
	padding: 20px;
	right: 0px;
	top: 3px;
`;
const copyView = {
	position: 'absolute',
	width: 65,
	height: 50,
	left: 20
};
const CopyBorderBottom = styled.View`
	top: 10px;
	margin-left: -5px;
	background-color: #14a09c;
	border-color: #14a09c;
	border-top-left-radius: 1.5px;
	border-bottom-left-radius: 1.5px;
	border-bottom-right-radius: 3px;
	height: 3px;
	width: 95%;
`;
const CopyBorderLeft = styled.View`
	left: 95%;
	margin-left: -8px;
	top: -3px;
	background-color: #14a09c;
	border-color: #14a09c;
	border-top-left-radius: 1.5px;
	border-top-right-radius: 1.5px;
	height: 10px;
	width: 3px;
`;

const DeleteBorderBottom = styled.View`
	top: 10px;
	margin-left: -5px;
	background-color: #d33851;
	border-color: #d33851;
	border-top-left-radius: 1.5px;
	border-bottom-left-radius: 1.5px;
	border-bottom-right-radius: 3px;
	height: 3px;
	width: 100%;
`;
const DeleteBorderLeft = styled.View`
	left: 100%;
	margin-left: -8px;
	top: -3px;
	background-color: #d33851;
	border-color: #d33851;
	border-top-left-radius: 1.5px;
	border-top-right-radius: 1.5px;
	height: 10px;
	width: 3px;
`;
const deleteView = {
	position: 'absolute',
	width: 75,
	height: 50,
	left: 90
}
const DelText = styled.Text`
	font-size: 20px;
	color: #14a09c;
	font-family: 'CentraNo2-Extrabold';
	top: 9px;
`;

const Token = styled.Text`
	font-size: 20px;
	color: #14a09c;
	font-family: 'CentraNo2-Extrabold';
	top: -9px;
`;
const TokenBorderBottom = styled.View`
	top: -12px;
	margin-left: -5px;
	background-color: #14a09c;
	border-color: #14a09c;
	border-top-left-radius: 1.5px;
	border-bottom-left-radius: 1.5px;
	border-bottom-right-radius: 3px;
	height: 3px;
	width: 90px;
`;
const TokenBorderLeft = styled.View`
	left: 90px;
	margin-left: -8px;
	top: -25px;
	background-color: #14a09c;
	border-color: #14a09c;
	border-top-left-radius: 1.5px;
	border-top-right-radius: 1.5px;
	height: 10px;
	width: 3px;
`;

const TextBorder = styled.View`
	left: 95px;
	margin-top: 0;
	height: 50px;
	border-color: #e8e8e9;
	border-bottom-right-radius: 24px;
	border-top-left-radius: 15px;
	border-top-right-radius: 24px;
`;
const TextLine = styled.View`
	top: 1px;
	margin-left: 7px;
	height: 3px;
	border-top-left-radius: 1.5px;
	border-top-right-radius: 1.5px;
	border-bottom-left-radius: 1.5px;
	border-bottom-right-radius: 1.5px;
`;


export default class Account extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
					token: "",
					above: false,
					rotate: 0,
					backgroundColor: props.dark ? '#626072' : "#e8e8e9",
					textColor: props.dark ? "#e1e3e3" : "#474d60",
					circleColor: props.dark ? "#525060" : "#f7f9f9",
					dotColor: props.dark ? "#e1e3e3" : "#20938c"
				}
				this.showAlert = this.showAlert.bind(this);
				this.toggleShow = this.toggleShow.bind(this);
				this.removePressed = this.removePressed.bind(this);
    }

	componentDidUpdate(prevProps) {
        if (prevProps.dark !== this.props.dark) {
            this.setState({
                backgroundColor: this.props.dark ? '#626072' : "#e8e8e9",
                textColor: this.props.dark ? "#e1e3e3" : "#474d60",
                circleColor: this.props.dark ? "#525060" : "#f7f9f9",
                dotColor: this.props.dark ? "#e1e3e3" : "#20938c"
            })
        }
        // Calculate percentage on props percentage update
		if (prevProps.percent !== this.props.percent) {
			this.setState({rotate: (this.props.percent*360)})
			if (this.props.percent == 0 || this.props.percent == 1){
				this.updateToken();
			}
		}
	}

	componentDidMount() {
        // Calculate token
		var token = totp(this.props.secret, { time: new Date() });
		token = token.slice(0,3) + " " + token.slice(3,)
		this.setState({token: token})

        // Recalculate token on state change e.g. opened app from background
		AppState.addEventListener('change', state =>
			{var token = totp(this.props.secret, { time: new Date() });
			token = token.slice(0,3) + " " + token.slice(3,)
			this.setState({token: token})}
	    )
	}

	updateToken = async () => {
		var token = totp(this.props.secret);
		token = token.slice(0,3) + " " + token.slice(3,)
		if (token != this.state.token){
			this.setState({token: token})
		}
	}

    // Delete alert
	showAlert() {
		Alert.alert(
			`Are you sure you want to remove ${this.props.title}${this.props.issuer && ':'}${this.props.issuer}?`,
			"Removing this account will mean you are no longer able to generate its codes.\n\nRemoving this account will not turn off 2-factor authentication, therefore make sure you:\n\n• Have removed 2-factor authentication from the source\n\nor\n\n• Have alternative way of accessing the codes\n\nOtherwise you will be unable to sign in!",
			[
		    {
		      text: 'Cancel',
		      onPress: () => console.log('Cancel Pressed'),
		      style: 'negative'
		    },
		    {text: 'REMOVE', onPress: () => this.removePressed()}
		  ],
  		{cancelable: true},
		)
	}

	removePressed = async () => {
		let bigobj = await Keychain.getGenericPassword()
		bigobj = bigobj.password
		bigobj = JSON.parse(bigobj)
		let secretlisttemp = bigobj.secretlist
		let computerlisttemp = bigobj.computerlist
		delete secretlisttemp[this.props.itemkey]
		delete computerlisttemp[this.props.itemkey]
		bigobj.secretlist = secretlisttemp
		bigobj.computerlist = computerlisttemp
		await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
		this.props.updateAccounts()
	}

    // Toggles showing the Copy and Delete buttons
	toggleShow() {
		this.setState({above: !this.state.above})
	}

	render() {
        // Here because we need to dynamically set the rotation
		this.BallDiv = styled.View`
			width: 40px;
			position: absolute;
			height: 40px;
			right: 0px;
			top: 3px;
			transform: rotate(${this.state.rotate}deg);
		`;
    	return (
			<>
				<View style={[{maxHeight: 50, width: '90%', padding: 10, height: 50, left: "5%"}]} >

					<Token>{this.state.token}</Token>
					<TokenBorderBottom />
					<TokenBorderLeft />
					<TouchableHighlight underlayColor={'transparent'} style={{color: 'red', width: '100%', marginTop:-50}} onPress={() => this.toggleShow()} onLongPress={this.props.dragreturn} >
						<TextBorder style={{backgroundColor: this.state.backgroundColor, width: Dimensions.get('window').width*0.9-100}}>
							{!this.state.above &&
								<>
									<Text style={{fontFamily: 'CentraNo2-Medium', fontSize: 20, top: 3, left: 8, color: this.state.textColor}}>{this.props.issuer.toLowerCase()}</Text>
									<TextLine style={{backgroundColor: this.state.textColor, borderColor: this.state.textColor, width: Dimensions.get('window').width-210}} />
									<Text style={{fontFamily: 'CentraNo2-Medium', fontSize: 10, top: 5, left: 8, color: this.state.textColor}}>{this.props.title}</Text>
								</>}
							{this.state.above &&
								<>
									<TouchableHighlight underlayColor={'transparent'} style={copyView} onPress={() => ToastAndroid.show("Copied", ToastAndroid.SHORT) & Clipboard.setString(totp(this.props.secret, { time: new Date() }))}>
										<View ><DelText>copy</DelText>
										<CopyBorderBottom />
										<CopyBorderLeft /></View>
									</TouchableHighlight>
									<TouchableHighlight underlayColor={'transparent'} style={deleteView} onPress={() => this.showAlert()}>
										<View ><DelText style={{color:'#d33851'}}>delete</DelText>
										<DeleteBorderBottom />
										<DeleteBorderLeft /></View>
									</TouchableHighlight>
								</>}

						</TextBorder>
					</TouchableHighlight>

					<CircleBig onPress={() => this.toggleShow()} style={{backgroundColor: this.state.circleColor}}></CircleBig>
					<this.BallDiv style={{}} onPress={() => this.toggleShow()}>
						<Text style={{fontSize: 30, color: this.state.dotColor, textAlign: 'center', top: -15}}>•</Text>
					</this.BallDiv>
				</View>
			</>
    );
  }
}
