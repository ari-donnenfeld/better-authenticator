import React from 'react';
import {Text, TouchableHighlight, ToastAndroid, Button, View, TextInput} from 'react-native';
import styled from 'styled-components'
import * as Keychain from 'react-native-keychain';
import totp from 'totp-generator'


const Banner = styled.View`
    background-color: #474d60;
    height: 55px;
`;

const BackButton = styled.Text`
    font-size: 35px;
    color: white;
    transform: rotate(180deg);
    left: -88%;
    top: 9px;
`;

const TitleText = styled.Text`
    font-size: 30px;
    color: white
    left: 30%;
    top: -39px;
    font-family: 'CentraNo2-Medium';
`;

const IssuerInput = styled.TextInput`
    border-width: 1px;
    width: 80%;
    left: 10%;
    border-radius: 10px;
    margin-top: 20px;
`;

const SaveText = styled.Text`
    font-size: 30px;
    color: #14a09c;
    font-family: 'CentraNo2-Extrabold';
    top: -5px;
`;
const SaveBorderBottom = styled.View`
    top: -5px;
    margin-left: -5px;
    background-color: #14a09c;
    border-color: #14a09c;
    border-top-left-radius: 1.5px;
    border-bottom-left-radius: 1.5px;
    border-bottom-right-radius: 3px;
    height: 3px;
    width: 85px;
`;
const SaveBorderLeft = styled.View`
    left: 85px;
    margin-left: -8px;
    top: -18px;
    background-color: #14a09c;
    border-color: #14a09c;
    border-top-left-radius: 1.5px;
    border-top-right-radius: 1.5px;
    height: 10px;
    width: 3px;
`;
const SaveTouch = styled.TouchableHighlight`
    position: absolute;
    width: 35%;
    height: 50px;
    right: 0%;
    bottom: 20px;
`;

export default class Scanner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userValue: "",
            issuerValue: "",
            secretValue: "",
            algorithm: "sha1",
            digits: "6",
            period: "30",
            siteValue: "",
            biggest: 0,
            textColor: props.dark ? '#f7f9f9' : "#525060",
            placeholderColor: props.dark? '#e1e6e6' : '#605e70'
        }
        this.onSuccess = this.onSuccess.bind(this);
        this.goBack = this.goBack.bind(this)
        this.handleSave = this.handleSave.bind(this)



    }
    onSuccess = (e) => {
        this.props.callBack.bind(this)(e.data)
    }
    goBack() {
        this.props.callBack.bind(this)('None')
    }

    handleSave= async () => {
        try {
            let keychainstr = await Keychain.getGenericPassword()
            let bigstr = keychainstr.password
            let bigobj = {}
            let secretlistadd = {}
            try {
                bigobj = JSON.parse(bigstr)
            } catch {
                bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
            }

            secretlistadd = bigobj.secretlist

            let count = Object.keys(secretlistadd)
            let biggest = 0
            for (let hi=0; hi<count.length;hi++) {
                if (count[hi] > biggest) {biggest = count[hi]}
            }

            if (this.state.secretValue.length < 5) {
                ToastAndroid.show("ERROR", ToastAndroid.SHORT)
                return;
            }
            // Try to get token. Try will catch errors
		    var token = totp(this.state.secretValue, { time: new Date() });

            secretlistadd[biggest+1] = {title: this.state.userValue, issuer: this.state.issuerValue, secret: this.state.secretValue, site: [this.state.siteValue], algorithm: this.state.algorithm, digits: this.state.digits, period: this.state.period}
            bigobj.secretlist = secretlistadd
            let computerlist = {}
            for (let key in bigobj.secretlist) {
                computerlist[key] = {issuer: bigobj.secretlist[key].issuer, site: bigobj.secretlist[key].site}
            }
            bigobj.computerlist = computerlist
            await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
            this.goBack();

        } catch (e) {
            ToastAndroid.show("ERROR", ToastAndroid.SHORT)
        }
    }

    render() {
        return (
            <View style={{height: '100%'}}>
            <Banner>
            <TouchableHighlight onPress={this.goBack}>
            <BackButton>{'➜'}</BackButton>
            </TouchableHighlight>
            <TitleText>add account:</TitleText>
            </Banner>

            <Text style={{textAlign: 'center', fontFamily: 'CentraNo2-Medium', marginBottom: -15, color: this.state.textColor}}>{'\n'}Issuer:</Text>
            <IssuerInput placeholder="e.g. Facebook" placeholderTextColor={this.state.placeholderColor} value={this.state.issuerValue} onChangeText={text => this.setState({issuerValue: text})} style={{textAlign: 'center', borderColor: this.state.textColor, color: this.state.textColor}} />

            <Text style={{textAlign: 'center', fontFamily: 'CentraNo2-Medium', marginBottom: -15, marginTop: 19, color: this.state.textColor}}>User:</Text>
            <IssuerInput placeholder="e.g. john.smith" placeholderTextColor={this.state.placeholderColor} value={this.state.userValue} onChangeText={text => this.setState({userValue: text})} style={{textAlign: 'center', borderColor: this.state.textColor, color: this.state.textColor}} />

            <Text style={{textAlign: 'center', fontFamily: 'CentraNo2-Medium', marginTop: -5, color: this.state.textColor}}>{"\n\n"}Site:</Text>
            <Text style={{textAlign: 'center', fontFamily: 'CentraNo2-Medium' , fontSize: 10, margin: 0, marginBottom: -15, color: this.state.textColor}}>Add site so the extention knows which it is.{"\n"}(This can be left blank if Site is the same as Issuer)</Text>
            <IssuerInput placeholder="e.g. facebook.com" placeholderTextColor={this.state.placeholderColor} value={this.state.siteValue} onChangeText={text => this.setState({siteValue: text})} style={{textAlign: 'center', borderColor: this.state.textColor, color: this.state.textColor}} />
            <Text style={{textAlign: 'center', fontFamily: 'CentraNo2-Medium', marginBottom: -15, color: this.state.textColor}}>{"\n\n"}Secret Key:</Text>
            <IssuerInput placeholder="e.g. V65NC2PYMFR7JFGS56..." placeholderTextColor={this.state.placeholderColor} value={this.state.secretValue} onChangeText={text => this.setState({secretValue: text})} style={{textAlign: 'center', borderColor: this.state.textColor, color: this.state.textColor}} />

            <SaveTouch underlayColor={'transparent'} onPress={this.handleSave}>
            <View ><SaveText>save</SaveText>
            <SaveBorderBottom />
            <SaveBorderLeft /></View>
            </SaveTouch>
            </View>

        );
    }
}
