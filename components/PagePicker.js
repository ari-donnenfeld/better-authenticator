import React from 'react';
import { View, ToastAndroid } from 'react-native';
import socketIOClient from "socket.io-client";
import CryptoJS from 'crypto-js';
import totp from 'totp-generator'
import Main from './Main';
import AddAccount from './AddAccount';
import Scanner from './Scanner'
import Popup from './Popup'
import messaging from '@react-native-firebase/messaging';
const auth = require('./google_auth_pb')
import base64 from 'react-native-base64';
import base32 from 'hi-base32';

import * as Keychain from 'react-native-keychain';

const Pages = {
    Main: 'Main',
    Scanner: 'Scanner',
    Popup: 'Popup',
    AddAccount: "AddAccount",
    None: "None"
};


export default class PagePicker extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            page: Pages.None,
			lastscanned: 'None',
			dark: false,
			connected: false,
			popupdata: typeof props.keyrequest == 'undefined' ? "None" : props.keyrequest
		}
		this.scannerCallBack = this.scannerCallBack.bind(this);
		this.mainCallBack = this.mainCallBack.bind(this);
		this.handleLogin = this.handleLogin.bind(this);
		this.popupCallBack = this.popupCallBack.bind(this);
		this.handleOtpauth = this.handleOtpauth.bind(this)
    }

    componentDidUpdate() {
        // Open popup if keyrequest is set
        if (this.props.keyrequest != null && this.state.page != Pages.Popup) {
            this.setState({page: Pages.Popup})
        }
    }

	componentDidMount = async () =>  {

		let keychainstr = await Keychain.getGenericPassword()
		let bigstr = keychainstr.password
		let bigobj = {}
		try {
			bigobj = JSON.parse(bigstr)
		} catch {
			bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
		}
		this.setState({dark: bigobj.constants.dark})
        // If chanel exists, set connected to true
		if (bigobj.login.chanel) {
			this.setState({connected: true})
			//this.forceUpdate()
		}

        // Open page only once keychain is read
        // This prevents dark mode flicker
        if (this.props.keyrequest != null) {
            this.setState({page: Pages.Popup})
        } else {
            this.setState({page: Pages.Main})
        }
	}

    //test = messaging().onNotificationOpenedApp(remoteMessage => {
        //// Notification cause app to open from background state
        //// Set state to popup with data
        //console.log("THIS IS ISNSIDE TESTING")
        //data = remoteMessage.data.keyrequest
        //this.setState({page: Pages.Popup, popupdata: data.substring(8,)})
    //});

    // On extention qr scan
	handleLogin = async (data) => {
        // Send over computerlist to computer
		try {
			let bigstr = await Keychain.getGenericPassword()
			bigstr = bigstr.password
			let bigobj = {}
			try {
				bigobj = JSON.parse(bigstr)
			} catch {
				bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
			}
			let login = {chanel: data.substring(data.indexOf(":")+1, data.indexOf("?")), password: data.substring(data.indexOf("?")+1,)}
			bigobj.login = login
			await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
			this.setState({connected: true})

            await messaging().subscribeToTopic(bigobj.login.chanel);
            let message = "connected&"+JSON.stringify(bigobj.computerlist)
            const socket = socketIOClient("https://gentle-hamlet-35362.herokuapp.com/");
            socket.emit("computer", {room: bigobj.login.chanel, message: CryptoJS.AES.encrypt(message, bigobj.login.password).toString()})

		} catch (e) {
			console.log("An error has occured")
		}
        // Set view back to main
		this.setState({page: Pages.Main})
	}

    // On totp qr scan
	handleOtpauth = async (data) => {
        // Convert totp QR data and save it
		let algorithm= 'sha1';
		let digits = '6'
		let period = '30';
		let issuer = ""
		let secret = ""
		data = decodeURIComponent(data)
		var post = data.substring(data.indexOf("?")+1,);

		var tempdata = post.split("&");
		var datasplit;
		for (var i=0;i<tempdata.length;i++) {
			datasplit = tempdata[i].split("=")
			if (datasplit[0] == "algorithm") algorithm = datasplit[1]
			if (datasplit[0] == "digits") digits = datasplit[1]
			if (datasplit[0] == "period") period = datasplit[1]
			if (datasplit[0] == "issuer") issuer = datasplit[1]
			if (datasplit[0] == "secret") secret = datasplit[1]

		}
		// console.log("Showing you the stuff i got:")
		// console.log("algorithm: " + algorithm);
		// console.log("period: " + period);
		// console.log("digits: " + digits);
		// console.log("algorithm: " + issuer);
		// console.log("period: " + secret);
		data = data.substring(data.indexOf("totp/")+5,)
		let userValue = data.substring(data.indexOf(":")+1, data.indexOf("?"))

		// If there is no issuer, it might be before the userValue
		if (!issuer) {
			if (data.indexOf(":") != -1) {
				issuer = data.substring(0,data.indexOf(":"))
			}
		}
        // Save the data
		try {
			let keychainstr = await Keychain.getGenericPassword()
			let bigstr = keychainstr.password
			let bigobj = {}
			let secretlistadd = {}
			try {
				bigobj = JSON.parse(bigstr)
			} catch {
				bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
			}
			secretlistadd = bigobj.secretlist
			let count = Object.keys(secretlistadd)
			let biggest = 0
			for (let hi=0; hi<count.length;hi++) {
				if (count[hi] > biggest) {biggest = count[hi]}
			}
			secretlistadd[parseInt(biggest)+1] = {title: userValue, issuer: issuer, secret: secret, site: [], algorithm: algorithm, digits: digits, period: period}
			bigobj.secretlist = secretlistadd
            // Create computerlist based on secret list, but remove secret key and userValue
			let computerlist = {}
			for (let key in bigobj.secretlist) {
				computerlist[key] = {issuer: bigobj.secretlist[key].issuer, site: bigobj.secretlist[key].site}
			}
			bigobj.computerlist = computerlist
            try {
                // Make sure it doesnt fail before saving
                totp(secret, { time: new Date() });
                await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
            } catch {
                ToastAndroid.show("Import failed", ToastAndroid.SHORT)
            }
		} catch {
            ToastAndroid.show("Import failed", ToastAndroid.SHORT)
		}
        // Keep this here instead of in the scannerCallBack.
        this.setState({page: Pages.Main})

	}
    // On revieve data from scanner (camera)
	scannerCallBack = async (data) => {
        // Chose what to do with data based on first characters in string

        // Google Authenticator Migration
        if (data.substring(0,17) == "otpauth-migration") {
            // This is not a great way of extracting paramaters
            data = data.substring(data.indexOf("=")+1,)
            data = decodeURIComponent(data)

            const desData = auth.MigrationPayload.deserializeBinary(data); 
            const jsonData = desData.toObject();

            let keychainstr = await Keychain.getGenericPassword()
            let bigstr = keychainstr.password
            let bigobj = {}
            try {
                bigobj = JSON.parse(bigstr)
            } catch {
                bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
            }
			let count = Object.keys(bigobj.secretlist)
			let biggest = 0
			for (let hi=0; hi<count.length;hi++) {
				if (count[hi] > biggest) {biggest = count[hi]}
			}

            for (var i=0; i< jsonData.otpParametersList.length; i++) {
                // Secret is in base64, but we need it in base32
                secret64 = jsonData.otpParametersList[i].secret
                secret = base64.decode(secret64)
                secret32 = base32.encode(secret, true)

                userValue = jsonData.otpParametersList[i].name
                userValue = userValue.substring(userValue.indexOf(":")+1,)
                issuer = jsonData.otpParametersList[i].issuer
                algorithm = 'sha1';
                digits = '6'
                period = '30';

                try {
                    // Make sure it doesnt fail before adding
		            totp(secret32, { time: new Date() });
                    bigobj.secretlist[parseInt(biggest)+1+i] = {title: userValue, issuer: issuer, secret: secret32, site: [], algorithm: algorithm, digits: digits, period: period}
                    bigobj.computerlist[parseInt(biggest)+1+i] = {issuer: issuer, site: []}
                } catch {
                    ToastAndroid.show("One or more imports failed!", ToastAndroid.SHORT)
                }
            }
            await Keychain.setGenericPassword('key', JSON.stringify(bigobj));

			this.setState({page: Pages.Main})

        // TOTP QR
        } else if (data.substring(0,7) == "otpauth") {
			this.handleOtpauth(data)
        // Better Authenticator QR
		} else if (data.substring(0,4) == "baut")  {
			this.handleLogin(data);
        // Unrecognised
		}	else {
			this.setState({page: Pages.Main})
		}
        // Save scanned data
		this.setState({lastscanned: data})
	}

    // Call back from Main.js
	mainCallBack= async (data) => {
        // Update to background color based on dark (in keychain)
		if (data == "dark"){
			let keychainstr = await Keychain.getGenericPassword()
			let bigstr = keychainstr.password
			let bigobj = {}
			try {
				bigobj = JSON.parse(bigstr)
			} catch {
				bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
			}
			this.setState({dark: bigobj.constants.dark})
        // Keyboard pressed in accounts. change to addaccount
		} else if (data == 'Text') {
            this.setState({page: Pages.AddAccount})
        // Logout pressed in post
		} else if (data == 'logout') {
            this.setState({page: Pages.Main})
			this.setState({connected: false})
        // Notification recieved 
		} else if (data.substring(0,12) == 'notification') {
            this.setState({page: Pages.Popup, popupdata: data.substring(12,)})
        // Camera
		} else {
            this.setState({page: Pages.Scanner})
		}
	}
    // Call back for popup (on cancel/exit)
	popupCallBack() {
        this.setState({page: Pages.Main})
        this.props.keyrequest = null
	}

	render() {
		return (
			<>
				<View>
					{this.state.page == Pages.Popup && <Popup dark={this.state.dark} data={this.state.popupdata} callBack={this.popupCallBack}/>}
					{this.state.page == Pages.Scanner && <Scanner callBack={this.scannerCallBack}/>}
					{this.state.page == Pages.Main && <Main connected={this.state.connected} dark={this.state.dark} pickerCallBack={this.mainCallBack} lastscanned={this.state.lastscanned}/>}
					{this.state.page == Pages.AddAccount && <AddAccount callBack={this.scannerCallBack} dark={this.state.dark} data={this.state.lastscanned} />}
				</View>
			</>
		);
	}
}
