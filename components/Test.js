import React from 'react';
import {Text, Alert, Switch, TouchableHighlight, ToastAndroid, Clipboard, View} from 'react-native';
import * as Keychain from 'react-native-keychain';
import styled from 'styled-components'

import JSEncrypt from './images/jsencrypt.min'

const ExportText = styled.Text`
    font-size: 35px;
    color: #14a09c;
    font-family: 'CentraNo2-Extrabold';
    top: 9px;
`;

const ExportTouch = styled.TouchableHighlight`
    textAlign: center;
    alignSelf: center;
`;

const ExportBorderBottom = styled.View`
    top: 10px;
    margin-left: -5px;
    background-color: #14a09c;
    border-color: #14a09c;
    border-top-left-radius: 2px;
    border-bottom-left-radius: 2px;
    border-bottom-right-radius: 4px;
    height: 4px;
    width: 132px;
`;

const ExportBorderLeft = styled.View`
    left: 131px;
    margin-left: -8px;
    top: -7px;
    background-color: #14a09c;
    border-color: #14a09c;
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
    height: 14px;
    width: 4px;
`;

export default class Test extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timeLeft: "23:59",
            exportState: "none",
            textColor: props.dark ? '#f7f9f9' : "#525060",
            dark: props.dark
        }

        this.getData = this.getData.bind(this);
        this.saveTime = this.saveTime.bind(this);
        this.exportPress = this.exportPress.bind(this);
        this.toggleDark = this.toggleDark.bind(this);
        this.exportView = this.exportView.bind(this);
        this.importPress = this.importPress.bind(this);

        this.getData()
    }


    // On dark toggle press
    toggleDark = async () =>  {
        // Toggle dark in keychain
        this.setState({dark: !this.state.dark})
        let keychainstr = await Keychain.getGenericPassword()
        let bigstr = keychainstr.password
        let bigobj = {}
        try {
            bigobj = JSON.parse(bigstr)
        } catch {
            bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
        }
        if (bigobj.constants.dark) {
            bigobj.constants["dark"] = false;
            this.setState({dark: false, textColor: "#525060"})
            this.setState({	textColor: "#525060"})
        } else {
            bigobj.constants["dark"] = true;
            this.setState({dark: true})
            this.setState({	textColor: '#f7f9f9'})
        }

        await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
        this.props.mainCallBack("dark")
    }


	importPress() {
		Alert.alert(
			`Import from Google Authenticator`,
            "• Open Google Authenticator\n• Press the three dots on the top right\n• Press 'Tranfer accounts'\n• Press 'Export accounts'\n• Select the accounts you want to import\n• Press 'Next'\n• Use the better authenticator scanner to scan the QR code\n• If its on the same phone:\n\t• Use another phone to take a photo of the QR code\n\t• Scan the image from the better authenticator scanner",
			[
		    {
		      text: 'Later',
		      onPress: () => console.log('OK Pressed'),
		      style: 'negative'
		    },{
                text: 'Scan',
                onPress: () => this.props.mainCallBack("camera"),
                style: 'positive'
            }
		  ],
  		{cancelable: true},
		)
	}

    callCamera() {
        this.props.mainCallBack("camera");
    }

    saveTime = async (time) =>  {
        // Saves current time in keychain (for the backup)
        let keychainstr = await Keychain.getGenericPassword()
        let bigstr = keychainstr.password
        let bigobj = {}
        try {
            bigobj = JSON.parse(bigstr)
        } catch {
            bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
        }
        bigobj.constants["exportTimeRequest"] = time;
        await Keychain.setGenericPassword('key', JSON.stringify(bigobj));

        this.setState({exportState: "wait"})
    }

    getData = async () => {
        const credentials = await Keychain.getGenericPassword();
        let bigobj = {}
        try {
            bigobj = JSON.parse(credentials.password)
        } catch {
            bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
        }
        exportTimeRequest = bigobj.constants.exportTimeRequest
        // Backup not pressed
        if (exportTimeRequest == undefined) {
            this.setState({exportState: "pre"})
        // Backup in process or done
        } else {
            var d = new Date();
            timeLeft = parseInt((exportTimeRequest+86400000 - d.getTime())/1000);
            // Too long since backup pressed
            if (timeLeft < -86500){ 
                // Over 24 hours after unlock. Resetting time.
                bigobj.constants.exportTimeRequest = undefined;
                await Keychain.setGenericPassword('key', JSON.stringify(bigobj));

                this.setState({exportState: "pre"})
            // over 24 hours after backup pressed but not over 48
            } else if (timeLeft < 0) {
                // We use phone date for smplicity when printing the time remaining.
                // But this is not secure to use; it can be easily changed
                // so when the time should be done, we check with the server for real time.

                // We can set the export state, as long as we set the PlainText Later
                this.setState({exportState: "export"})

                public_key = "-----BEGIN PUBLIC KEY-----\n"+
                    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuvhHmTQsq2JGn2xDBC87\n"+
                    "P+VIc2mpo4nMSzqE4WgQAmcB55M8LZuKBNY+UsE7iu+XUhPLn7qppGY1qtQ/De2Y\n"+
                    "qSZw3TBKFKFaBuwa9fOS4D1V5ZXDDOI6uUHi3xsBANKdY1dHwEGg0vrvIBWBXDTG\n"+
                    "wh28Myab/zesqFbqIYhbh9SZMyv30U3EmrAWXFSCmWM+dSfuIahmkYbNLe1RmxVw\n"+
                    "/s2YrFc/YG2BQabZbAZM8mcPGyjvr+zay0SQxwwhyzjehPij/j8wJ0C4yJ7JF324\n"+
                    "J+Qv6UvHaEOznVN1v0hiQUcChCk0b3KkyEmVNBTBe2k9nYzioL+XuYLp6cys5rtS\n"+
                    "CQIDAQAB\n"+
                    "-----END PUBLIC KEY-----";

                const jsEncrypt = new JSEncrypt();
                jsEncrypt.setPublicKey(public_key);
                randomString = Math.random().toString(36).replace(/[^a-z]+/g, '')
                encrypted = jsEncrypt.encrypt(randomString)

                fetch("https://gentle-hamlet-35362.herokuapp.com//time?challenge=" + encodeURIComponent(encrypted))
                    .then((response) => response.json())
                    .then((responseJson) => {
                        if (responseJson["decryptedText"] == randomString) {
                            // This means no funny business has occured
                            // 1 day in epoch (seconds): 86400
                            // Note that time given in getTime is epoch in miliseconds!
                            timeLeft = parseInt((exportTimeRequest+86400000 - parseInt(responseJson["time"]))/1000);
                            if (timeLeft < 0) {
                                // It really has been 24 hours
                                this.setState({exportPlainText: JSON.stringify(JSON.parse(credentials.password).secretlist)})
                            } else {
                                ToastAndroid.show("Error. Make sure the phone time is correct.", ToastAndroid.LONG)
                            }
                        } else {
                            ToastAndroid.show("Error. Make sure you are connected to the internet.", ToastAndroid.LONG)
                        }
                    })
                    .catch((error) => {
                        ToastAndroid.show("Error. Make sure you are connected to the internet.", ToastAndroid.LONG)
                    })


            // Less than 24 hours have passed. Keep waiting.
            }else {
                hoursLeft = parseInt(timeLeft/3600)
                minutesLeft =  parseInt((timeLeft-hoursLeft*3600)/60)
                this.state.timeLeft = hoursLeft + ":" + minutesLeft
                this.setState({exportState: "wait"})
            }
        }
    }


    // Actual data view. On view after the 24 hours of export
    exportView() {
        Alert.alert(
            `Save the following text somewhere secure:`,
            `${this.state.exportPlainText}`,
            [
                {
                    text: 'Close',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'negative'
                },
                {text: 'Copy', onPress: () => ToastAndroid.show("Copied", ToastAndroid.SHORT) & Clipboard.setString(this.state.exportPlainText)}
            ],
            {cancelable: true},
        )
    }

    // Export button pressed
    exportPress() {
        public_key = "-----BEGIN PUBLIC KEY-----\n"+
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuvhHmTQsq2JGn2xDBC87\n"+
            "P+VIc2mpo4nMSzqE4WgQAmcB55M8LZuKBNY+UsE7iu+XUhPLn7qppGY1qtQ/De2Y\n"+
            "qSZw3TBKFKFaBuwa9fOS4D1V5ZXDDOI6uUHi3xsBANKdY1dHwEGg0vrvIBWBXDTG\n"+
            "wh28Myab/zesqFbqIYhbh9SZMyv30U3EmrAWXFSCmWM+dSfuIahmkYbNLe1RmxVw\n"+
            "/s2YrFc/YG2BQabZbAZM8mcPGyjvr+zay0SQxwwhyzjehPij/j8wJ0C4yJ7JF324\n"+
            "J+Qv6UvHaEOznVN1v0hiQUcChCk0b3KkyEmVNBTBe2k9nYzioL+XuYLp6cys5rtS\n"+
            "CQIDAQAB\n"+
            "-----END PUBLIC KEY-----";

        const jsEncrypt = new JSEncrypt();
        jsEncrypt.setPublicKey(public_key);
        randomString = Math.random().toString(36).replace(/[^a-z]+/g, '')
        encrypted = jsEncrypt.encrypt(randomString)

        // Get the real time
        fetch("https://gentle-hamlet-35362.herokuapp.com/time?challenge=" + encodeURIComponent(encrypted))
            .then((response) => response.json())
            .then((responseJson) => {
                // Challenge passed (server was able to decrypt the random string)
                if (responseJson["decryptedText"] == randomString) {
                    // This means no funny business has occured
                    // Send current time to be saved
                    this.saveTime(responseJson["time"])
                    // 1 day in epoch (seconds): 86400
                    // Note that time given in getTime is epoch in miliseconds!
                    var d = new Date();
                    timeLeft = parseInt((parseInt(responseJson["time"])+86400000 - d.getTime())/1000);
                    hoursLeft = parseInt(timeLeft/3600)
                    minutesLeft =  parseInt((timeLeft-hoursLeft*3600)/60)
                    this.state.timeLeft = hoursLeft + ":" + minutesLeft

                } else {
                    // The challenge failed
                    ToastAndroid.show("Error. Make sure you are connected to the internet.", ToastAndroid.LONG)
                }
            })
            .catch((error) => {
                ToastAndroid.show("Error. Make sure you are connected to the internet.", ToastAndroid.LONG)
            })


    }

    render() {
        return (
            <>
            <Text style={{left: 20, top: 20, color: this.state.textColor}}>Better Authenticator version 1.2</Text>

            <Text style={{fontSize: 20, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', marginBottom: -20, left: 20, marginTop: 50}}>Dark mode</Text>
            <Switch
            onValueChange = {this.toggleDark}
            value = {this.state.dark}
            style={{right: 20}}/>

            {(this.state.exportState == "wait") && <>
                <Text style={{fontSize: 20, marginTop: 50, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Export Requested.</Text>
                <Text style={{fontSize: 20, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Avaiable in:</Text>
                <Text style={{fontSize: 50, color: '#14a09c', fontFamily: 'CentraNo2-Extrabold', textAlign: 'center'}}>{this.state.timeLeft}</Text>
                </>}

            {(this.state.exportState == "pre") && <>
                    <Text style={{fontSize: 20, marginTop: 50, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Export Keys:</Text>
                    <Text style={{fontSize: 10, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Once requested you will have to wait 24 hours, and then have 24 hours to extract it. This is to prevent someone quckly swiping your phone and exporting the keys.</Text>
                    <ExportTouch underlayColor={'transparent'} onPress={this.exportPress}>
                    <View ><ExportText>export</ExportText>
                    <ExportBorderBottom />
                    <ExportBorderLeft /></View>
                    </ExportTouch>
                    </>}

            {(this.state.exportState == "export") && <>
                    <Text style={{fontSize: 20, marginTop: 50, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Export Keys:</Text>
                    <Text style={{fontSize: 10, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Be very careful with these keys. You can copy them here, but make sure to copy something else once you are done. Ideally, save this to a file and print it out. Then edit the file and change the text to gibberish before deleting it.</Text>
                    <ExportTouch underlayColor={'transparent'} onPress={this.exportView}>
                    <View ><ExportText>view</ExportText>
                    <ExportBorderBottom style={{width: 96}}/>
                    <ExportBorderLeft style={{left: 95.1}}/></View>
                    </ExportTouch>

                    </>}

                <Text style={{fontSize: 20, marginTop: 50, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', textAlign: 'center'}} >Import keys:</Text>
                <Text style={{fontSize: 10, color: this.state.textColor, fontFamily: 'CentraNo2-Medium', maxWidth: '80%', left: '10%', textAlign: 'center'}} >You can import keys from Google Authenticator!</Text>
                <ExportTouch underlayColor={'transparent'} onPress={this.importPress}>
                <View ><ExportText>import</ExportText>
                <ExportBorderBottom />
                <ExportBorderLeft /></View>
                </ExportTouch>

            <Text style={{position: 'absolute', bottom: 100, fontSize: 14, color: this.state.textColor, textAlign: 'center', fontFamily: 'CentraNo2-Medium', left: '10%', width: '80%'}}>
            {"\n\n"}
            If you find any bugs, or want to request any features please go to the website contact form at:{"\n"}betterauthenticator.tk/
            </Text>
            </>

        );
    }
}
