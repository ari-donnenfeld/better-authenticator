import React from 'react';
import {Text, Image, ToastAndroid, TouchableHighlight, View} from 'react-native';
import socketIOClient from "socket.io-client";
import messaging from '@react-native-firebase/messaging';
import CryptoJS from 'crypto-js';
import * as Keychain from 'react-native-keychain';
import styled from 'styled-components'
import Connect from './images/small_connect.png'
import ConnectDark from './images/small_connect_dark.png'

const LogText = styled.Text`
    font-size: 20px;
    color: #d33851;
    font-family: 'CentraNo2-Extrabold';
    top: 9px;
`;
const LogTouch = styled.TouchableHighlight`
    position: absolute;
    height: 50px;
    right: 30px;
    bottom: 90px;
`;
const SyncTouch = styled.TouchableHighlight`
    position: absolute;
    width: 35%;
    height: 50px;
    left: 30px;
    bottom: 90px;
`;
const LogBorderBottom = styled.View`
    top: 10px;
    margin-left: -5px;
    background-color: #d33851;
    border-color: #d33851;
    border-top-left-radius: 1.5px;
    border-bottom-left-radius: 1.5px;
    border-bottom-right-radius: 3px;
    height: 3px;
    width: 85px;
`;
const LogBorderLeft = styled.View`
    left: 85px;
    margin-left: -8px;
    top: -3px;
    background-color: #d33851;
    border-color: #d33851;
    border-top-left-radius: 1.5px;
    border-top-right-radius: 1.5px;
    height: 10px;
    width: 3px;
`;

export default class HomePre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            textColor: props.dark ? '#f7f9f9' : "#525060"
        }
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout = async (data) => {
        try {
            let bigstr = await Keychain.getGenericPassword()
            bigstr = bigstr.password
            let bigobj = {}
            try {
                bigobj = JSON.parse(bigstr)
            } catch {
                bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
            }
            // Remove login information from keychain
            bigobj.login = {}
            await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
            ToastAndroid.show("Logged out.\nYou will have to manually log out of the extension.", ToastAndroid.SHORT)
            this.props.mainCallBack("logout")
        } catch (e) {
            console.log("An error has occured")
        }
    }


    // Sync button pressed
    handleSync = async () => {
        ToastAndroid.show("Remember to open extention before pressing sync", ToastAndroid.SHORT)
        let computerlist = {}
        let bigobj = {}
        try {
            let bigstr = await Keychain.getGenericPassword()
            bigstr = bigstr.password
            try {
                bigobj = JSON.parse(bigstr)
                computerlist = bigobj.computerlist
            } catch {
                console.log("Dont mind me")
            }
        } catch (e) {
            console.log("Dont mind me")
        }
        // Send the sync signal (save as connect signal)
        await messaging().subscribeToTopic(bigobj.login.chanel);
        let message = "connected&"+JSON.stringify(computerlist)
        const socket = socketIOClient("https://gentle-hamlet-35362.herokuapp.com/");
        socket.emit("computer", {room: bigobj.login.chanel, message: CryptoJS.AES.encrypt(message, bigobj.login.password).toString()})
    }

    render() {
        return (
            <>
                <Text style={{marginTop: 50, fontFamily: 'CentraNo2-Medium', color: this.state.textColor, textAlign: 'center', fontSize: 30}}>Connected!</Text>

                <Image style={{width: 150, height: 150, alignSelf: 'center', marginLeft: -15}} source={this.props.dark ? ConnectDark : Connect}/>

                <Text style={{width: '90%', left: '5%', marginTop: 50, fontFamily: 'CentraNo2-Medium', color: this.state.textColor, textAlign: 'center', fontSize: 20}}>Simply request a key on your computer and a popup will show.{"\n"}Theres no need to have this app open anymore. But remeber to re-sync every time you add another key.</Text>

                <SyncTouch underlayColor={'transparent'} onPress={this.handleSync}>
                    <View ><LogText style={{color: "#14a09c"}}>sync</LogText>
                    <LogBorderBottom style={{width: 58, borderColor: "#14a09c", backgroundColor: "#14a09c"}}/>
                    <LogBorderLeft style={{left: 58, borderColor: "#14a09c", backgroundColor: "#14a09c"}}/></View>
                </SyncTouch>

                <LogTouch underlayColor={'transparent'} onPress={this.handleLogout}>
                    <View ><LogText>log out</LogText>
                    <LogBorderBottom />
                    <LogBorderLeft /></View>
                </LogTouch>
            </>
        );
    }
}
