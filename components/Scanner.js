import React from 'react';
import {Text, TouchableHighlight, View, Dimensions} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styled from 'styled-components'
const totalwidth =  Dimensions.get('window').width
const totalheight =  Dimensions.get('window').height

const Banner = styled.View`
    background-color: #525060;
    height: 55px;
`;
const BackButton = styled.Text`
    font-size: 35px;
    color: white;
    transform: rotate(180deg);
    left: -88%;
    top: 9px;
`;
const TitleText = styled.Text`
    font-size: 30px;
    color: white
    left: 30%;
    top: -39px;
    font-family: 'CentraNo2-Medium';
`;
const TheBox = styled.View`
    width: 100%;
    height: 100%;
    border: 20px solid rgba(0, 0, 0, 0.5);
    border-left-width: ${totalwidth*0.10}px;
    border-right-width: ${totalwidth*0.10}px;
    border-top-width: ${(totalheight-55-totalwidth*0.8)/2}px;
    border-bottom-width: ${(totalheight-55-totalwidth*0.8)/2}px;

`;

export default class Scanner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.onSuccess = this.onSuccess.bind(this);
        this.goBack = this.goBack.bind(this)
    }
    // On scan
    onSuccess = (e) => {
        this.props.callBack.bind(this)(e.data)
    }
    // On cancel
    goBack() {
        this.props.callBack.bind(this)('None')
    }
    render() {
        return (
            <>
                <Banner>
                <TouchableHighlight onPress={this.goBack}>
                <BackButton>{'➜'}</BackButton>
                </TouchableHighlight>
                <TitleText>Scan Qr Code</TitleText>
                </Banner>
                <QRCodeScanner showMarker={true} customMarker={
                    <TheBox >
                    <View style={{width: '100%', height: 1, backgroundColor: '#d33851', top: '50%'}} />
                    </TheBox>
                } cameraStyle={{ height: Dimensions.get('window').height-55, marginTop: 0, width: '100%', alignSelf: 'center', justifyContent: 'center' }} onRead={this.onSuccess} />
            </>

        );
    }
}
