import React from 'react';
import {Image, Text, TouchableHighlight, View, TouchableOpacity} from 'react-native';
import DraggableFlatList from 'react-native-draggable-flatlist';
import Account from './Account';
import styled from 'styled-components'
import * as Keychain from 'react-native-keychain';
import Expand from './images/small_expand.png'
import ExpandDark from './images/small_expand_dark.png'
import Camera from './images/small_camera.png'
import CameraDark from './images/small_camera_dark.png'
import Keyboard from './images/small_keyboard.png'
import Connect from './images/small_connect.png'
import ConnectDark from './images/small_connect_dark.png'
import Settings from './images/small_settings.png'
import SettingsDark from './images/small_settings_dark.png'


const AddButton = styled.TouchableHighlight`
	width: 70px;
	height: 70px;
	borderRadius: 60px;
	border-width: 0;
	paddingTop: 0;
	paddingLeft: 0;
	position: absolute;
	right: 20px;
	bottom: 100px;
`

const AdditionDiv = styled.View`
	width: 80px;
	height: 220px;
	border-radius: 60px;
  	border-top-right-radius: 35px;
	border-top-left-radius: 0;
	padding-top: 19px;
	right: 15px;
	position: absolute;
	bottom: 95px;
`

export default class Accounts extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            textColor: props.dark ? '#f7f9f9' : "#525060",
			showplus: false,
            welcome: false,
			expandDivBackground: props.dark ? "#626072" : "#e8e8e9",
			expandBackground: props.dark ? "#525060" : "#f7f9f9",
			accountData: [],
			percent: 0
		}
		this.toggleAdd = this.toggleAdd.bind(this);
		this.getData = this.getData.bind(this);
		this.toggleCamera = this.toggleCamera.bind(this);
		this.toggleText = this.toggleText.bind(this);
		this.update = this.update.bind(this);
		this.dragEnd = this.dragEnd.bind(this);
        // Get data on start
		this.getData()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.dark !== this.props.dark) {
            this.setState({
                textColor: this.props.dark ? '#f7f9f9' : "#525060",
                expandDivBackground: this.props.dark ? "#626072" : "#e8e8e9",
                expandBackground: this.props.dark ? "#525060" : "#f7f9f9"
            })
        }

    }

	componentDidMount() {
        // Setup timer for changing percent every second
        // Clear perviously set timers first
		clearInterval(this.intervalID);
		this.intervalID = setInterval(
			() => this.tick(),
			1000
		);
	}

	componentWillUnmount() {
        // Clear the timer interval before closing
		clearInterval(this.intervalID);
	}

    // Done every second, calculates new percentage based on time
	tick() {
		let percent = 0
		var seconds = new Date().getSeconds();
		seconds > 30 ? percent = (seconds-30)/30 : percent = seconds/30
		this.setState({percent: percent})
	}

    // Simply gets the data
	update() {
		this.getData()
	}

    // On + Press, toggles showing camera...
	toggleAdd() {
		this.setState({showplus: !this.state.showplus})
	}

    // On Camera press
	toggleCamera() {
		this.props.mainCallBack.bind(this)("Camera")
	}
    // On Keybord press
	toggleText() {
		this.props.mainCallBack.bind(this)("Text")
	}
    // Gets data from keychain
	getData = async () => {
		let alldatastr = await Keychain.getGenericPassword()
		alldatastr = alldatastr.password
		let alldataobj = {}
		try {
			alldataobj = JSON.parse(alldatastr).secretlist
		} catch {
			alldataobj = {}
		}
		let data_as_array = Object.values(alldataobj)
		let keys_as_array = Object.keys(alldataobj)
		let accountData = []

        // Create account data array for every account in keychain
		for (var i=0; i<data_as_array.length; i++) {;
			accountData.push({key: keys_as_array[i], issuer: data_as_array[i].issuer, title: data_as_array[i].title, secret: data_as_array[i].secret, algorithm: data_as_array[i].algorithm, digits: data_as_array[i].digits, period: data_as_array[i].period});
		}
        // Show welcome screen if there are no accounts
        if (accountData.length == 0) {
            this.setState({welcome: true})
        }
        // Save the array of account to state
		this.setState({accountData: accountData})
	}

    // Converts account array to HTML element
	renderAccount = ({ item, index, drag, isActive }) => {
		return (
			<TouchableOpacity onLongPress={drag} style={{marginTop: 25, paddingTop: 3, shadowOffset: { width: 10, height: 10 },
			overflow: 'visible',
			shadowColor: 'black',
			shadowOpacity: 1,
			shadowOffset: { width: 10, height: 10 },
			elevation: isActive ? 3 : 0,
			// background color must be set
			backgroundColor : this.props.expandDivBackground
			  }} >
				<Account percent={this.state.percent} dragreturn={drag} dark={this.props.dark} key={item.key} itemkey={item.key} updateAccounts={this.update} issuer={item.issuer} title={item.title} secret={item.secret} algorithm={item.algorithm} digits={item.digits} period={item.period}/>
			</TouchableOpacity>
		)
	};

    // Once you let go of the account/drop it
	dragEnd = async (data) => {
        // Change the order of the accounts in keychain on drop
		if (JSON.stringify(data) != JSON.stringify(this.state.accountData)) {
			var newnumlist = []
			var oldnumlist = []
			for (var i=0;i<data.length;i++) {
				oldnumlist.push(this.state.accountData[i].key)
				newnumlist.push(data[i].key)
			}
			this.setState({accountData: data})
			let bigobj = await Keychain.getGenericPassword()
			bigobj = bigobj.password
			bigobj = JSON.parse(bigobj)
			let newsecretlisttemp = {}
			let newcomputerlisttemp = {}
			for (var i=0;i<oldnumlist.length;i++) {
				newsecretlisttemp[oldnumlist[i]] = bigobj.secretlist[newnumlist[i]]
				newcomputerlisttemp[oldnumlist[i]] = bigobj.computerlist[newnumlist[i]]
			}
			bigobj.secretlist = newsecretlisttemp
			bigobj.computerlist = newcomputerlisttemp
			await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
		}


	};

 	render() {
	 	return (
			<>
                {this.state.welcome && <View>

                    <Text style={{marginTop: 50, fontFamily: 'CentraNo2-Medium', color: this.state.textColor, textAlign: 'center', fontSize: 30}}>Welcome to{"\n"}<Text style={{fontFamily: 'CentraNo2-Extrabold'}}>better authenticator</Text>!</Text>

                    <Image style={{left: '5%', width: 50, height: 50, margin: 0, marginTop: 40}} source={this.props.dark ? ConnectDark : Connect}/>
                    <Text style={{width: '75%', left: '20%', marginTop: -50, fontFamily: 'CentraNo2-Medium', color: this.state.textColor, fontSize: 20}}>Connection screen:{"\n"}Sync to browser extention</Text>
                    <Image style={{left: '5%', width: 50, height: 50, margin: 0, marginTop: 40}} source={this.props.dark ? SettingsDark : Settings}/>
                    <Text style={{width: '75%', left: '20%', marginTop: -50, fontFamily: 'CentraNo2-Medium', color: this.state.textColor, fontSize: 20}}>Setting screen:{"\n"}Toggle Dark Mode{"\n"}Export Keys{"\n"}Import from Google Authenticator</Text>
                    <Image style={{left: '5%', width: 50, height: 50, margin: 0, marginTop: 40}} source={this.props.dark ? ExpandDark : Expand}/>
                    <Text style={{width: '75%', left: '20%', marginTop: -50, fontFamily: 'CentraNo2-Medium', color: this.state.textColor, fontSize: 20}}>Add button:{"\n"}Scan Authentication QR's{'\n'}Type them in manually</Text>

                </View>}

				<DraggableFlatList
					style={{height: '80%', flexGrow:0}}
					data={this.state.accountData}
					renderItem={this.renderAccount}
				 	keyExtractor={(item, index) => `${item.key}`}
					onDragEnd={({ data }) => this.dragEnd(data) }
					extraData={this.state.percent}
				/>

				{this.state.showplus && <AdditionDiv underlayColor={'transparent'} style={{backgroundColor: this.state.expandDivBackground}}/>}
				{this.state.showplus && <AddButton underlayColor={'transparent'} onPress={this.toggleCamera} style={{	bottom: 230, backgroundColor: 'none'}}><Image style={{width: '90%', height: '90%', left: '5%'}} source={this.props.dark ? CameraDark : Camera}/></AddButton>}
				{this.state.showplus && <AddButton underlayColor={'transparent'} onPress={this.toggleText} style={{	bottom: 170, backgroundColor: 'none'}}><Image style={{width: '90%', height: '90%', left: '5%'}} source={Keyboard}/></AddButton>}

				<AddButton underlayColor={'transparent'} style={{backgroundColor: this.state.expandBackground}} onPress={this.toggleAdd}>
					<Image style={{width: '100%', height: '100%'}} source={this.props.dark ? ExpandDark : Expand}/>
				</AddButton>
			</>
    );
  }
}
