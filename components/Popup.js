import React from 'react';
import {Text, ToastAndroid, TouchableHighlight, View} from 'react-native';
import Account from './Account';
import * as Keychain from 'react-native-keychain';
import CryptoJS from 'crypto-js';
import totp from 'totp-generator'
import styled from 'styled-components'
import socketIOClient from "socket.io-client";


const Title = styled.Text`
    font-size: 30px;
    text-align: center;
`;

const SendText = styled.Text`
font-size: 20px;
    color: #14a09c;
    font-family: 'CentraNo2-Extrabold';
    top: 9px;
`;

const SendTouch = styled.TouchableHighlight`
    position: absolute;
    width: 60px;
    left: 40%;
    bottom: 90px;
    margin-top: 10px;
    top: 35px;
    height: 40px;
`;

const SendBorderBottom = styled.View`
    top: 10px;
    margin-left: -5px;
    background-color: #14a09c;
    border-color: #14a09c;
    border-top-left-radius: 1.5px;
    border-bottom-left-radius: 1.5px;
    border-bottom-right-radius: 3px;
    width: 60px;
    height: 3px;
`;

const SendBorderLeft = styled.View`
    left: 60px;
    margin-left: -8px;
    top: -3px;
    background-color: #14a09c;
    border-color: #14a09c;
    border-top-left-radius: 1.5px;
    border-top-right-radius: 1.5px;
    height: 10px;
    width: 3px;
`;

const FillTouch = styled.TouchableHighlight`
    position: absolute;
    width: 60px;
    left: 65%;
    bottom: 90px;
    margin-top: 10px;
    top: 35px;
    height: 40px;
`;

const FillBorderBottom = styled.View`
    top: 10px;
    margin-left: -5px;
    background-color: #146da0;
    border-color: #146da0;
    border-top-left-radius: 1.5px;
    border-bottom-left-radius: 1.5px;
    border-bottom-right-radius: 3px;
    width: 50px;
    height: 3px;
`;

const FillBorderLeft = styled.View`
    left: 50px;
    margin-left: -8px;
    top: -3px;
    background-color: #146da0;
    border-color: #146da0;
    border-top-left-radius: 1.5px;
    border-top-right-radius: 1.5px;
    height: 10px;
    width: 3px;
`;

const DelText = styled.Text`
    font-size: 20px;
    color: #14a09c;
    font-family: 'CentraNo2-Extrabold';
    top: 9px;
`;

const DeleteBorderBottom = styled.View`
    top: 10px;
    margin-left: -5px;
    background-color: #d33851;
    border-color: #d33851;
    border-top-left-radius: 1.5px;
    border-bottom-left-radius: 1.5px;
    border-bottom-right-radius: 3px;
    height: 3px;
    width: 50px;
`;

const DeleteBorderLeft = styled.View`
    left: 50px;
    margin-left: -8px;
    top: -3px;
    background-color: #d33851;
    border-color: #d33851;
    border-top-left-radius: 1.5px;
    border-top-right-radius: 1.5px;
    height: 10px;
    width: 3px;
`;

const LogTouch = styled.TouchableHighlight`
    position: absolute;
    bottom: 15px;
    left: 75%;
`;

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        let encname = props.data
        let rtype = encname.substring(0,encname.indexOf(":"))

        encname = encname.substring(encname.indexOf(":")+1,)
        encname = encname[0].toUpperCase() + encname.slice(1)
        this.state = {
            name: "None",
            props: props,
            encname: encname,
            textColor: props.dark ? '#f7f9f9' : "#525060",
            percent: 0,
            displayArray: []
        }

        this.cancelHandler = this.cancelHandler.bind(this)
        this.sendHandler = this.sendHandler.bind(this)
        this.getData(rtype);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.dark !== this.props.dark) {
            this.setState({textColor: this.props.dark ? '#f7f9f9' : "#525060"})
        }

    }

	componentDidMount() {
        // Setup timer for changing percent every second
        // Clear perviously set timers first
		clearInterval(this.intervalID);
		this.intervalID = setInterval(
			() => this.tick(),
			1000
		);
	}

	componentWillUnmount() {
        // Clear the timer interval before closing
		clearInterval(this.intervalID);
	}

    // Done every second, calculates new percentage based on time
    tick() {
        let percent = 0
        var seconds = new Date().getSeconds();
        seconds > 30 ? percent = (seconds-30)/30 : percent = seconds/30
        this.setState({percent: percent})
    }


    getData = async (rtype) => {
        let alldatastr = await Keychain.getGenericPassword()
        alldatastr = alldatastr.password
        let alldataobj = {}
        this.setState({loginChanel: JSON.parse(alldatastr).login.chanel, loginPassword: JSON.parse(alldatastr).login.password})
        var decname;
        if (this.state.name == "None") {
            if (rtype == "Assoc" || rtype == "AssocFill") {
                decname = CryptoJS.AES.decrypt(this.state.encname, JSON.parse(alldatastr).login.password).toString(CryptoJS.enc.Utf8)
                this.setState({ name: decname.substring(0,decname.indexOf(":")) })
            } else {
                this.setState({ name: CryptoJS.AES.decrypt(this.state.encname, JSON.parse(alldatastr).login.password).toString(CryptoJS.enc.Utf8)})
            }
        }

        try {
            alldataobj = JSON.parse(alldatastr).secretlist
        } catch {
            alldataobj = {}
        }

        let data_as_array = Object.values(alldataobj)
        let keys_as_array = Object.keys(alldataobj)

        let displayArray = [];
        let array_to_send;
        for (var i=0; i<data_as_array.length; i++) {

            let against = data_as_array[i].issuer + " " + data_as_array[i].site
            if (against.toLowerCase().includes(this.state.name.toLowerCase())) {
                data_as_array[i].key = keys_as_array[i]
                // This whole Assoc thing is done terribly. Redo it from the ground up later.
                if (rtype == "Assoc"  || rtype == "AssocFill") {
                    data_as_array[i].assoc = true
                } else { 
                    data_as_array[i].assoc = false
                }
                array_to_send = data_as_array[i]
                displayArray.push(data_as_array[i])
            }
        }
        this.setState({displayArray: displayArray})
        // Only 1 found, we can send
        if (displayArray.length == 1 && rtype == "Fill") {
            this.sendHandler(array_to_send, "f")
            ToastAndroid.show("Filled", ToastAndroid.SHORT) 
        // Only 1 found, we can send
        } else if (displayArray.length == 1 && rtype == "AssocFill") {
            this.sendHandler(array_to_send, "f")
            ToastAndroid.show("Filled and Associated", ToastAndroid.SHORT) 
        // Multiple found so person has to chose which one to fill/associate
        } else if (rtype == "Fill" || rtype == "AssocFill") {
            ToastAndroid.show("Multiple found.", ToastAndroid.SHORT) 
        }
    }



    // Add site to site list of account
    associateSite = async (accountKey) => {
        let keychainstr = await Keychain.getGenericPassword()
        let bigstr = keychainstr.password
        let bigobj = {}
        try {
            bigobj = JSON.parse(bigstr)
        } catch {
            bigobj = {login: {}, secretlist: {}, computerlist: {}, constants: {}}
        }
        var decname = CryptoJS.AES.decrypt(this.state.encname, bigobj.login.password).toString(CryptoJS.enc.Utf8)
        decname = decname.substring(decname.indexOf(":")+1,)
        bigobj.secretlist[accountKey].site.push(decname)
        bigobj.computerlist[accountKey].site.push(decname)
        await Keychain.setGenericPassword('key', JSON.stringify(bigobj));
    }


    // Handle the send
    sendHandler(account_array, stype) {
        // Save the Association if requested
        if (account_array.assoc) {
            this.associateSite(account_array.key)
        }
        var token = totp(account_array.secret);
        let message;
        //s means to send it, f means to send and fill
        if (stype == "s") {
            message = "totp:" + token.toString()
        } else {
            message = "fotp:" + token.toString()
        }
        // Send the token
        const socket = socketIOClient("https://gentle-hamlet-35362.herokuapp.com/");
        socket.emit("computer", {room: this.state.loginChanel, message: CryptoJS.AES.encrypt(message, this.state.loginPassword).toString()})
        return;
    }

    // On handle press
    cancelHandler() {
        // Send back to pagePicker
        this.props.callBack()
    }

    render() {
        return (
            <View style={{height: '100%', backgroundColor: this.props.dark ? "#525060" : "#f7f9f9"}}>
                <Title style={{color: this.state.textColor, fontFamily: 'CentraNo2-Extrabold'}} >{"\n"}A request for the key: "{this.state.name}" has been made.</Title>
                <Title style={{color: this.state.textColor, fontSize: 25, fontFamily: 'CentraNo2-Extrabold'}}>{"\n" + this.state.displayArray.length} {this.state.displayArray.length > 1 ? "matches" : "match"} found:</Title>
                {
                    this.state.displayArray.map(item => (
                        <View style={{marginBottom: 30}}>
                        <View style={{width: '100%', marginBottom: 20}}>
                        <Account percent={this.state.percent} dark={this.props.dark} key={item.key} number={item.key} issuer={item.issuer} title={item.title} secret={item.secret} algorithm={item.algorithm} digits={item.digits} period={item.period}/>
                        </View>
                        <SendTouch onPress={() => this.sendHandler(item, "s")}>
                        <View><SendText style={{color:'#14a09c'}}>send</SendText>
                        <SendBorderBottom />
                        <SendBorderLeft /></View>
                        </SendTouch>
                        <FillTouch  onPress={() => this.sendHandler(item, "f")}>
                        <View><SendText style={{color:'#146da0'}}>fill</SendText>
                        <FillBorderBottom />
                        <FillBorderLeft /></View>
                        </FillTouch>
                        </View>
                    ))
                }
                <LogTouch onPress={this.cancelHandler.bind(this)}>
                    <View ><DelText style={{color:'#d33851'}}>exit</DelText>
                    <DeleteBorderBottom />
                    <DeleteBorderLeft /></View>
                </LogTouch>
            </View>
        );
    }
}
